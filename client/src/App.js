import React, { Component } from 'react';
import './App.css'
import { BrowserRouter as Router, Route, Link } from 'react-router-dom'
import Login from './Components/Login'
import Signup from './Components/SignUp'
import Home from './Components/Home'
import UserProfile from './Components/UserProfile'
import ChatRoom from './Components/ChatRoom'
import '../src/Assets/css/bootstrap.min.css'

function App() {
	return (
		<Router>
			<Route path="/login" exact component={Login} />
			<Route path="/signup" exact component={Signup} />
			<Route path="/" exact component={Home} />
			<Route path="/profile/:profileName" component={UserProfile} />
			<Route path="/message/:uuid?" component={ChatRoom} />
		</Router>
	);
}

export default App;
