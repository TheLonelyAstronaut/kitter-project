export function timeout(time, promise)
{
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            reject(new Error("Timeout error!"));
        }, time);
        promise.then(resolve, reject);
    });
}

export async function getServerResponse(url, body, out = 30000) {
    let result = {}
    let status = 0

    await fetch('https://kitter-server.herokuapp.com' + url, {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json'
        },
        body: body
    }).then(res => {
        //console.log(res)
        status = res.status
        let response = {}
        
        try{
            response = res.json()
        }finally{}
        
        return response
    }).then(res => {
        console.log(res, 'RES!');
        result = res;
        result.statusCode = status
    })
    .catch(err => {
        console.log(err)
        throw err
    })
    
    return result
}