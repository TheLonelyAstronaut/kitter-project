import * as Network from './NetworkBasics'

export async function loginByEmail(login, password){
    console.log(login, password)
    let response = await Network.getServerResponse('/auth/signin', JSON.stringify({
        username: login,
        password: password
    }))

    return response
}

export async function register(login, email, password, firstName, lastName){
    let response = await Network.getServerResponse('/auth/signup', JSON.stringify({
        username: login,
        password: password,
        first_name: firstName,
        last_name: lastName,
        email: email
    }))

    return response
}

export async function loginByToken(token, login){
    let response = await Network.getServerResponse('/auth/validate_token', JSON.stringify({
        token: token,
        username: login
    }))

    return response   
}

export async function logout(token, login){
    let response = await Network.getServerResponse('/auth/logout', JSON.stringify({
        token: token,
        username: login
    }))

    return response
}

export async function getUserProfile(token, username, query) {
    let response = await Network.getServerResponse('/users/get_profile', JSON.stringify({
        token: token,
        username: username,
        query: query
    }));

    return response;
}