import * as Network from './NetworkBasics'

export async function searchUser(query, login, token) {
    let response = await Network.getServerResponse('/users/search', JSON.stringify({
        username: login,
        token: token,
        query: query
    }))

    return response
}

export async function follow(login, token, query) {
    let response = await Network.getServerResponse('/users/follow', JSON.stringify({
        username: login,
        token: token,
        query: query
    }))

    return response
}

export async function enterChatRoom(login, token, companion) {
    let response = await Network.getServerResponse('/message/enter_room', JSON.stringify({
        username: login, 
        token: token,
        companion: companion
    }))

    return response
}