import * as Network from './NetworkBasics'

export async function addPost(login, token, content) {
    let response = await Network.getServerResponse('/posts/add', JSON.stringify({
        username: login,
        token: token,
        content: content
    }))

    return response
}

export async function sendLikeRequest(postid, login, token){
    //console.log(postid, userid)
    let response = await Network.getServerResponse('/posts/like', JSON.stringify({
        uuid: postid,
        username: login,
        token: token
    }))

    return response
}

export async function deletePost(postid, login, token) {
    let response = await Network.getServerResponse('/posts/delete', JSON.stringify({
        uuid: postid,
        username: login,
        token: token
    }))

    return response
}

export async function getFeed(token, login){
    let response = await Network.getServerResponse('/posts/feed', JSON.stringify({
        token: token,
        username: login
    }))

    return response
}


export async function getMessages(username, token){
    let response = await Network.getServerResponse('/message/get', JSON.stringify({
        username: username,
        token: token
    }))

    return response 
}

export async function getChatById(login, token, uuid){
    let response = await Network.getServerResponse('/message/get_current', JSON.stringify({
        username: login,
        uuid: uuid,
        token: token
    }))

    return response 
}