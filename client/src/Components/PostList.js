import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import "react-responsive-carousel/lib/styles/carousel.min.css";
import { Confirm } from 'semantic-ui-react'
import * as PostsAPI from '../API/Posts'

export default class PostList extends Component {
    constructor(props) {
        super(props);

        this.state = {
            posts: null,
            allowToPost: false,
            pictures: [],
            displayAddImagesSection: 'none',
            displayAddHashTagForm: 'none',
            images: [],
            postOptionHover: false,
            postToDelete: -1,
            openDeleteConfirm: false,
            hashtag: '',
            hashtags: [],
            userAvatar: null
        }
    }

    windowOnClick = () => {
        if (!this.state.postOptionHover) {
            let optionDropdowns = document.getElementsByClassName("dropdown-options");

            if (optionDropdowns.length != 0) {
                optionDropdowns.forEach(element => {
                    element.style.display = "none";
                })
            }
        }
    }


    componentDidUpdate(prevProps) {
        if (prevProps !== this.props) {
            this.setState({ posts: this.props.posts, allowToPost: this.props.allowToPost, userAvatar: this.props.userAvatar });

            window.addEventListener("click", this.windowOnClick, true);
        }
    }

    componentWillUnmount() {
        window.removeEventListener("click", this.windowOnClick, true);
    }

    handleLike = (e, postUUID) => {
        let changeStyle = (e) => {
            let likeNumber = document.getElementById("like-" + postUUID);
            let numberOfLikes;

            try {
                numberOfLikes = Number(likeNumber.innerHTML.split(' ')[0]);
            } catch{
                return;
            }

            likeNumber.innerHTML = e.target.classList.contains('outline') ? (
                numberOfLikes + 1 + (numberOfLikes + 1 > 1 ? ' Likes' : ' Like')
            ) : (
                    numberOfLikes - 1 + (numberOfLikes - 1 > 1 ? ' Likes' : ' Like')
                );

            e.target.classList.contains('outline') ?
                e.target.classList.add('red') :
                e.target.classList.remove('red');

            e.target.classList.contains('outline') ?
                e.target.classList.remove('outline') :
                e.target.classList.add('outline');
        }

        changeStyle(e)

        const login = localStorage.getItem('login');
        const token = localStorage.getItem('token');

        PostsAPI.sendLikeRequest(postUUID, login, token)
            .then(res => {
                if (res.statusCode != 200) {
                    changeStyle(e)
                }
            }).catch(err => {
                changeStyle(e)
            })
    }

    handleCreatePost = async (e) => {
        e.preventDefault();

        const login = localStorage.getItem('login');
        const token = localStorage.getItem('token');

        PostsAPI.addPost(login, token, {
            text: this.state.text_content
        })
        .then(async (res) => {
            this.setState({ text_content: '' })
            if (res.statusCode == 200) await this.props.requestPosts()
        }).catch(err => { })
    }

    onDrop = (pictureFiles) => {
        this.setState({ pictures: pictureFiles });
    }

    showPostOption = (e) => {
        let optionDropdowns = document.getElementsByClassName("dropdown-options");
        for (let i = 0; i < optionDropdowns.length; i++) {
            optionDropdowns[i].style.display = "none";
        }
        let dropdown = e.target.nextSibling;
        dropdown.style.display = dropdown.style.display == "block" ? "none" : "block";
    }

    deletePost = () => {
        this.setState({ openDeleteConfirm: false })
        //Call API
        const login = localStorage.getItem('login');
        const token = localStorage.getItem('token');

        PostsAPI.deletePost(this.state.postToDelete, login, token)
            .then(res => {
                console.log(res)
                if (res.statusCode === 200) {
                    let newPosts = this.state.posts;

                    for (let i = 0; i < newPosts.length; i++)
                        if (newPosts[i].uuid === this.state.postToDelete) {
                            newPosts.splice(i, 1);
                            break;
                        }

                    this.setState({ posts: newPosts })
                }
            }).catch(err => { })
    }

    render() {
        const email = localStorage.getItem('email');
        const posts = this.state.posts === null ? '' :
            this.state.posts.map((post, index) => {
                let likedByMe = post.liked;

                const likeButtonClass = !likedByMe ? 'like icon outline link' : 'red like icon link';

                const postDropdown = post.user.username === localStorage.getItem('profile_name') ? (
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="notification-dropdown">
                        <div onClick={() => this.setState({ postToDelete: post.uuid, openDeleteConfirm: true })} class="dropdown-item" href="#">Delete this post</div>
                    </div>
                ) : null;

                return (
                    <div className="container post-item">
                        {
                            post.user.username === localStorage.getItem('login') ? (
                                <a style={{ cursor: 'pointer' }} aria-haspopup="true" aria-expanded="false" role="button" id="notification-dropdown" data-toggle="dropdown" className="float-right"><i class="triangle down icon"></i></a>
                            ) : null
                        }
                        {
                            postDropdown
                        }
                        <div className='row post-header'>
                            <div className="col-sm-2 col-lg-1 post-avatar-column">
                                <img className='ui image avatar' src={require('../Assets/images/avatar.jpg')} />
                            </div>
                            <div className='col-sm-10 col-lg-10 post-user-name-column'>
                                <Link to={'/profile/' + post.user.username} className="post-user-name">{post.user.first_name + ' ' + post.user.last_name}</Link>
                                <div className="post-time">{post.date_created}</div>
                            </div>
                        </div>
                        <div className="post-content">
                            <p>{post.content.trim()}</p>
                        </div>
                        <div className="extra">
                            <div class="ui right labeled button post-like">
                                <button style={{ padding: '0 .7em' }} class="ui icon button post-like-button" tabindex="0">
                                    <i onClick={(e) => this.handleLike(e, post.uuid)} aria-hidden="true" className={likeButtonClass}></i>
                                </button>
                                {
                                    <a id={"like-" + post.uuid} style={{ fontSize: '.85714286rem' }} class="ui left pointing basic label">{post.likes + (post.likes > 1 ? ' Likes' : ' Like')}</a>
                                }
                            </div>
                        </div>
                    </div>
                )
            });

        const createNewPost = this.state.allowToPost ? (
            <div className='post-input-section'>
                <div className="ui segment active tab" style={{ backgroundColor: 'rgb(246, 215, 191)' }}>
                    <form className="ui form create-post-form" onSubmit={this.handleCreatePost} method="post">
                        <div className='fields' style={{ marginBottom: 35 }}>
                            <img className='create-post-user-avatar rounded-circle' src={require('../Assets/images/avatar.jpg')} />
                            <textarea value={this.state.text_content} required onChange={e => this.setState({ text_content: e.target.value })} style={{ resize: 'none', marginLeft: 10, backgroundColor: 'rgb(252, 240, 228)' }} placeholder="Tell us more" rows="3"></textarea>
                        </div>
                        <div className="field">
                            <input style={{ float: 'right', marginTop: -30 }} type='submit' className="button ui primary-color" value="Post" />
                        </div>
                    </form>
                </div>
            </div>
        ) : '';

        const post_contents = this.state.posts == null ? (
            <div className=''>
                {
                    createNewPost
                }
                <div className="ui active centered inline loader my-2"></div>
            </div>
        ) : (
                <div className=''>
                    {
                        createNewPost
                    }
                    {
                        posts
                    }
                </div>
            )

        return (
            <div className="post-list">
                {
                    post_contents
                }
                <Confirm
                    open={this.state.openDeleteConfirm}
                    content='Are you sure you want to delete this post?'
                    onCancel={() => this.setState({ openDeleteConfirm: false })}
                    onConfirm={this.deletePost}
                />
            </div>
        );
    }
}