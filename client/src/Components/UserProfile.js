import React, {Component} from 'react';
import Navbar from './Navbar'
import PostList from './PostList'
import SideBar from './Sidebar'
import 'cropperjs/dist/cropper.css';
import * as AuthAPI from '../API/Auth'
import * as UsersAPI from '../API/Users'

export default class UserProfile extends Component {
    constructor(props) {
        super(props);

        this.feedTab = React.createRef();
        this.followersTab = React.createRef();
        this.followingsTab = React.createRef();
        this.feedTabPanel = React.createRef();
        this.followersTabPanel = React.createRef();
        this.followingsTabPanel = React.createRef();
        
        this.state = {
            user: null,
            user_posts: null,
            allowToPost: false,
            requestUserIsAnonymous: false,
            userData: null,
            canChat: false,
            pictures: [],
            picData: null,
            aspect: 1/1,
            cropAvatarUploadActive: false,
            croppedAvatar: null,
            followings: [],
            followers: [],
            userAvatar: null,
            isFollowing: null
        }
    }

    async UNSAFE_componentWillMount() {
        await this.checkAuth()
        await this.requestPosts();
    }

    async componentDidUpdate(prevProps) {
        if(prevProps !== this.props)
        {
            await this.checkAuth();
            await this.requestPosts();
            this.state.isFollowing = null;
        }
    }

    checkAuth = async () => {
        const login = localStorage.getItem('login');
        const token = localStorage.getItem('token');

        AuthAPI.loginByToken(token, login)
        .then(res => {
            if (res.statusCode != 200) {
                localStorage.clear();
                this.props.history.push('/login');
            }
            else {
                console.log(res)

                this.setState({
                    userData: res, 
                    followings: res.data.followings
                });
            }
        }).catch(err => {})
    }

    requestPosts = async (navigateToMenu = true) => {
        const selfLogin = localStorage.getItem('login');
        const token = localStorage.getItem('token');
        const login = this.props.match.params.profileName;

        AuthAPI.getUserProfile(token, selfLogin, login)
        .then(async res => {
            console.log(res);
            if (res.statusCode === 200) {
                let posts = [];
                
                if(res.posts) {
                    res.posts.forEach(post => {
                        let mainPost = post[0];
                        mainPost.likes = post[1];
                        mainPost.liked = post[2];
                        posts.push(mainPost);
                    })
                }

                if(navigateToMenu) {
                    let isFollowing = false;

                    if(res.followers) {
                        res.followers.map(f => {
                            if(f.username == selfLogin) {
                                isFollowing = true;
                            }
                        })
                    }

                    try{
                        this.setState({isFollowing: isFollowing})
                    }catch{}
                }

                this.setState({user_posts: posts, user: res.user, followings: res.following, followers: res.followers})

                if(login == selfLogin) this.setState({allowToPost: true})
                else this.setState({allowToPost: false})

                if(!this.state.allowToPost && !this.state.requestUserIsAnonymous) this.setState({canChat: true})
                else this.setState({canChat: false})

                if(navigateToMenu) this.feedTab.current.click()
            }
        }).catch(err => {})
    }

    handleFollow = (e) => {
        const login = localStorage.getItem('login');
        const token = localStorage.getItem('token');
        const profileName = this.props.match.params.profileName;

        UsersAPI.follow(login, token, profileName)
        .then(res => {
            if (res.statusCode === 200)
            {
                e.target.innerHTML = e.target.classList.contains('border') ?  `Followed &nbsp;<i className="check disabled icon"></i>` :
                'Follow';

                if (e.target.classList.contains('border'))
                    e.target.classList.remove('border');
                else
                    e.target.classList.add('border');

                this.requestPosts(false)
            }
        }).catch(err => {})
    }

    startChat = () => {
        const login = localStorage.getItem('login');
        const token = localStorage.getItem('token');
        const profileName = this.props.match.params.profileName;

        UsersAPI.enterChatRoom(login, token, profileName)
        .then(res => {
            if(res.statusCode == 200) {
                let uuid = res.uuid;
                this.props.history.push('/message/' + uuid); 
            }
        }).catch(err => {})
    }
  
    render() {
        const followButton = this.state.allowToPost ? '' : this.state.isFollowing == null ? (
            <button className="ui loading button follow-button">Loading</button> 
        ) : this.state.isFollowing ? (
            <button onClick={e => {e.persist(); this.handleFollow(e)}} className='ui blue button follow-button'>Followed</button> 
        ): (
            <button onClick={e => {e.persist(); this.handleFollow(e)}} className='ui border blue button follow-button'>Follow</button>
        )

        const startChatButton = this.state.canChat ? (
            <button className='ui red button' onClick={this.startChat}><i aria-hidden="true" className="chat icon"></i> Message</button>
        ) : '';

        const followers = this.state.user == null ? '' : 
                        this.state.user.followers == undefined ? '' : 
                        this.state.user.followers.length + (
                            this.state.user.followers.length > 1 ? ' Followers' : ' Follower'
                        );
        
        const user_name = this.state.user == null ? '' : (
            this.state.user.first_name + ' ' + this.state.user.last_name
        );
        
        const uploadAvatarButton = this.state.picData != null ? (
            <button className="ui blue button" onClick={this.handleUploadAvatar}>Upload</button>
        ) : '';
        
        const userInfo = this.state.user == null ? '' : (
            <div role="list" className="ui list">
                <div role="listitem" className="item">
                    <i aria-hidden="true" className="marker icon"></i>
                    <div className="content">Minsk, Belatus</div>
                </div>
                <div role="listitem" className="item">
                    <i aria-hidden="true" className="mail icon"></i>
                    <div className="content"><a href={"mailto:"+this.state.user.email}>{this.state.user.email}</a></div>
                </div>
            </div>
        )

        return (
            <div className='background'>
                <Navbar userData={this.state.userData} history={this.props.history}/>
                <div className='profile-container'>
                    <div className='row'>
                        <div className='d-none d-md-block col-lg-3'>
                            <SideBar userData={this.state.userData} activeClass="profile-item" />
                        </div>
                        <div className="col-sm-12 col-lg-3 order-lg-9">
                            <div className="ui card">
                                <div className="image avatar primary-color">
                                    <img className='profile-avatar' src={require('../Assets/images/avatar.jpg')} />
                                </div>
                                <div className="content primary-color">
                                    <div style={{textAlign: 'center'}} className="header">
                                        {
                                            user_name
                                        }
                                    </div>
                                    <div className="meta"><span className="date">
                                        {
                                            userInfo
                                        }
                                        </span>
                                    </div>
                                    <div className="description">I'm a fullstack web developer</div>
                                </div>
                                <div className="extra content primary-color">
                                    <a><i aria-hidden="true" className="user icon"></i>{followers}</a>
                                    <br />
                                    {
                                        followButton
                                    }
                                    {
                                        startChatButton
                                    }
                                </div>
                            </div>
                        </div>

                    <div className="col-sm-12 col-lg-6 order-lg-5">
                        <ul class="nav nav-tabs nav-fill" id="profile-tab" role="tablist">
                            <li class="nav-item">
                                <a ref={this.feedTab} class="nav-link active primary-color" id="feed-tab" data-toggle="tab" href="#nav-feed" role="tab" aria-controls="feed" aria-selected="true">Feed</a>
                            </li>
                            <li class="nav-item">
                                <a ref={this.followingsTab} class="nav-link primary-color" id="following-tab" data-toggle="tab" href="#nav-following" role="tab" aria-controls="following" aria-selected="false">Following</a>
                            </li>
                            <li class="nav-item">
                                <a ref={this.followersTab} class="nav-link primary-color" id="followers-tab" data-toggle="tab" href="#nav-followers" role="tab" aria-controls="followers" aria-selected="false">Followers</a>
                            </li>
                        </ul>

                        <div class="tab-content" id="profile-tabContent">
                        <div ref={this.feedTabPanel} id="nav-feed" class="tab-pane fade show active">
                            <PostList userAvatar = {this.state.userAvatar} posts={this.state.user_posts} allowToPost={this.state.allowToPost} user={this.state.user} requestPosts={this.requestPosts} />
                        </div>
                        <div ref={this.followingsTabPanel} id="nav-following" class="tab-pane fade">
                            <div role="list" className="ui list item">
                                {this.state.followings?.map(fl =>(
                                    <div onClick={() => {
                                        this.props.history.push("/profile/" + fl.username)
                                    }} role="listitem" className="item">
                                        <img src={require('../Assets/images/avatar.jpg')} className="ui avatar image" />
                                        <div className="content">
                                        <div className="header">{fl.first_name + ' ' + fl.last_name}</div>
                                            {fl.username}
                                        </div>
                                    </div>
                                ))}
                            </div>
                        </div>

                        <div ref={this.followersTabPanel} id="nav-followers" class="tab-pane fade">
                            <div role="list" className="ui list item">
                                {this.state.followers?.map(fl =>(
                                    <div onClick={() => this.props.history.push("/profile/" + fl.username)} role="listitem" className="item">
                                        <img src={require('../Assets/images/avatar.jpg')} className="ui avatar image" />
                                        <div className="content">
                                        <div className="header">{fl.first_name + ' ' + fl.last_name}</div>
                                            {fl.username}
                                        </div>
                                    </div>
                                ))}
                            </div>
                        </div> 
                        </div>
                    </div>

                    </div>
                </div>
            </div>
        );
    }
  }