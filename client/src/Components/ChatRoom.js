import React, {Component} from 'react';
import Navbar from './Navbar'
import { Popup } from 'semantic-ui-react'
import io from 'socket.io-client';
import * as AuthAPI from '../API/Auth'
import * as PostsAPI from '../API/Posts'

export default class ChatRoom extends Component {
    static chatSocket = null;

    constructor(props) {
      super(props);
      this.state = {
        user: {},
        user_posts: null,
        allowToPost: false,
        requestUserIsAnonymous: true,
        messages: null,
        connectionEstablished: false,
        userData: null,
        lastMessages: null
      }
    }

    checkAuth = async () => {
        const login = localStorage.getItem('login');
        const token = localStorage.getItem('token');

        AuthAPI.loginByToken(token, login)
        .then(res => {
            if(res.statusCode != 200){
                localStorage.clear(); 
                this.props.history.push('/login');    
                return;
            }


            this.getLastMessages();
        })
        .catch(err => {
            localStorage.clear(); 
            this.props.history.push('/login');
        })
    }

    getLastMessages = () => {
        PostsAPI.getMessages(localStorage.getItem('login'), localStorage.getItem('token'))
        .then(res => {
            console.log(res)
            if(res.statusCode == 200) {
                res = res.chats;
                let lastMessages = []

                if(res) {
                    res.forEach(chat => {
                        let newChat = [];
                        newChat.user = chat[1];
                        newChat.uuid = chat[0];
                        newChat.text = chat[2] ? chat[2] : "No messages yet";

                        lastMessages.push(newChat);
                    })
                }

                this.setState({lastMessages: lastMessages});
            }
        })        
        .catch(err => {})
    }

    getChatMessages = () => {
        PostsAPI.getChatById(localStorage.getItem('login'), localStorage.getItem('token'), this.props.match.params.uuid)
        .then(res => {
            if(res.statusCode == 200) {
                this.setState({messages: res.messages});
                
                if(this.msgList) {
                    this.msgList.scrollTop = this.msgList.scrollHeight;
                }
            }
        })
        .catch(err => {})
    }

    componentWillUnmount() {
        if(this.chatSocket) {
            this.chatSocket.on('close_room', (msg) => {
                if(msg.statusCode == 200) {
                    this.chatSocket.disconnect()
                }
            })

            this.chatSocket.emit('close_room', {
                token: localStorage.getItem('token'),
                username: localStorage.getItem('login')
            })
        }
    }

    connectToSocket = async () => {
        let receiver = this.props.match.params.uuid;
        console.log(receiver);

        this.chatSocket = io.connect('https://kitter-server.herokuapp.com/');

        this.chatSocket.on('connect', (msg) => { 
            if(msg?.statusCode === 200) {
                this.chatSocket.emit('create_room', {
                    token: localStorage.getItem('token'),
                    username: localStorage.getItem('login')
                });
                this.setState({connectionEstablished: true})
            }   
        })

        this.chatSocket.on('new_message', (msg) => {
            if(msg) {
                let message = JSON.parse(msg.message)
                
                if(message.chat.uuid == this.props.match.params.uuid) {
                    this.state.messages.push(message)
                }

                let isNewUser = true;

                this.state.lastMessages.forEach(currentMessage => {
                    if(currentMessage.uuid == message.chat.uuid) {
                        currentMessage.text = message.content
                        isNewUser = false;
                    }
                })

                if(isNewUser) {
                    this.state.lastMessages.unshift({
                        text: message.content,
                        uuid: message.chat.uuid,
                        user: message.user
                    })
                }

                this.forceUpdate()
                
                if(this.msgList) {
                    this.msgList.scrollTop = this.msgList.scrollHeight;
                }
            }
        })
    }

    componentDidMount() {
        this.checkAuth();
        this.connectToSocket();

        if(this.props.match.params.uuid) {
            this.getChatMessages()
        }else{
            this.setState({messages: null});
        }
    }

    async componentDidUpdate(prevProps) {
        if(prevProps.match.params.uuid !== this.props.match.params.uuid)
        {
            if(this.props.match.params.uuid) {
                this.setState({messages: null})
                this.getChatMessages()
            }
        }
    }

    componentWillUnmount() {
        if (this.chatSocket != null && this.chatSocket != undefined) this.chatSocket.close()
    }


    sendChatMessage = (e) => {
        e.preventDefault();

        if(this.chatSocket) {
            this.chatSocket.emit('new_message', {
                username: localStorage.getItem('login'),
                token: localStorage.getItem('token'),
                text: this.state.message,
                uuid: this.props.match.params.uuid
            })
        }

        this.setState({message: ''})

    }

    goToRoom = (uuid) => {
        this.props.history.push('/message/' + uuid);
    }

    render() {
        const messageList = this.state.messages == null ? '' : this.state.messages.map((msg) => {
            if(msg.user.username == localStorage.getItem('login'))
                return (
                    <div role="listitem" className="item chat-message-right">
                        <div className="content">
                            <div className="description">
                            {
                                msg.content
                            }
                            </div>
                        </div>
                        <Popup content={msg.user.first_name + ' ' + msg.user.last_name} position='top right'
                        trigger={<img onClick={() => this.props.history.push('/profile/' + msg.user.username)} src={require('../Assets/images/avatar.jpg')} className="ui avatar image"/>} />     
                    </div>
                )
            else return (
                <div role="listitem" className="item chat-message-left">
                    <Popup content={msg.user.first_name + ' ' + msg.user.last_name} position='top left'
                            trigger={<img onClick={() => this.props.history.push('/profile/' + msg.user.username)} src={require('../Assets/images/avatar.jpg')} className="ui avatar image"/>} />     
                    <div className="content">
                        <div className="description">
                        {
                            msg.content
                        }
                        </div>
                    </div>
                </div>
            )
        });
        
        const lastMessages = this.state.lastMessages == null ? (
            <div style={{marginTop: '.5em'}} className="ui active centered inline loader"></div>
        ) : this.state.lastMessages.length == 0 ? (
            <div style={{marginTop: '.5em'}}></div>
        ) : this.state.lastMessages.map(chat => (
            <div onClick={() => this.goToRoom(chat.uuid)} className="item chat-room-list-item">
                <div className="ui tiny image">
                    <img src={require('../Assets/images/avatar.jpg')}/>
                </div>
                <div className="content">
                    <a className="header">{chat.user.first_name + ' ' + chat.user.last_name}</a>
                    <div className="description">
                        <p>{chat.text}</p>
                    </div>
                </div>
            </div>
        ))

        const body = !this.state.connectionEstablished ? <div style={{marginTop: '.5em'}}  className="ui active centered inline loader"></div> : (
            <div className='chat-container row'>
                <div className='col-3 chat-column-1'>
                    <div className='chat-column-2-header row'>
                        <h3>Your Conversations</h3>
                    </div>
                    <div className="ui items chat-room-list">
                    {
                        lastMessages
                    }
                    </div>
                </div>
                <div className='col-1 wide column'></div>
                {
                    this.state.messages == null ? (
                        <div className='col-8 chat-column-2'>
                            <div className='chat-column-2-header'>
                                <h3>Messages</h3>
                            </div>
                        </div>
                    ) : this.state.messages.length == 0 && this.props.match.params.uuid == undefined ? (
                        <div className='col-8 chat-column-2'>
                            <div className='chat-column-2-header'>
                                <h3>Messages</h3>
                            </div>
                            <div className="ui warning message" style={{backgroundColor: 'rgb(252, 240, 228)'}}>
                                <div className="header">You don't have any conversations!</div>
                                <p>Start making some friends!</p>
                            </div>
                        </div>
                    ) : (
                        <div className='col-8 chat-column-2'>
                            <div className='chat-column-2-header'>
                                <h3>Messages</h3>
                            </div>
                            <div ref={(ref) => this.msgList = ref} role="list" id='chat-message-section' className="ui very relaxed list chat-message-section">
                            {
                                messageList
                            }
                            </div>
                            <div className=''>
                                <form className='ui form send-message-form row' onSubmit={this.sendChatMessage}>
                                    <div className="field col-11">
                                        <input className='ui input' value={this.state.message} onChange={e => this.setState({message: e.target.value})}/>
                                    </div>
                                    <div className="field col-1"><button className="ui icon button"><i aria-hidden="true" className="send icon"></i></button></div>
                                </form>
                            </div>
                        </div>
                    )
                }
            </div>
        );

        return (
            <div className='chat-wrapper'>
                <Navbar getLastMessages={this.getLastMessages} userData={this.state.userData} history={this.props.history}/>
                {
                    body
                }
            </div>
        )
    }
  }