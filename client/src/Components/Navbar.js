import React, { Component } from 'react';
import { Link } from 'react-router-dom'
import logo from '../Assets/images/logo4.png'
import * as AuthAPI from '../API/Auth'
import * as UsersAPI from '../API/Users'

window.onresize = () => {
    let sidebar = document.getElementById("sidebar");
    let trendingHashTags = document.getElementById("trending-hashtags");

    if (sidebar != null) {
        sidebar.style.width = sidebar.parentElement.clientWidth - 32 + 'px';
    }

    if (trendingHashTags != null) {
        trendingHashTags.style.width = trendingHashTags.parentElement.clientWidth - 32 + 'px';
    }
}

export default class Navbar extends Component {
    constructor(props) {
        super(props);
        this.state = {
            requestUserIsAnonymous: null,
            notifications: null,
            newNotifications: 0,
            chatRooms: [],
            displayResult: 'none',
            searchResult: [],
            searchResultHover: false,
            menuPosition: 'static',
        }
        this.timer = null;
    }

    handleLogout = () => {
        AuthAPI.logout(localStorage.getItem('token'), localStorage.getItem('login'));
        localStorage.clear();

        this.props.history.push('/login');
    }

    searchAutocomplete = (e) => {
        this.setState({ searchQuery: e.target.value })

        clearTimeout(this.timer);

        this.timer = setTimeout(() => {
            UsersAPI.searchUser(this.state.searchQuery, localStorage.getItem('login'), localStorage.getItem('token'))
            .then(res => {
                res = res.users;
                let users = [];
                if (res) {
                    res.forEach(user => {
                        let newUser = user[0];
                        newUser.isFollowing = user[1];
                        users.push(newUser);
                    })
                }

                this.setState({ searchResult: users })

                if (this.state.searchQuery == '') this.setState({ displayResult: 'none' })
                else this.setState({ displayResult: users.length == 0 ? 'none' : 'block' })
            }).catch(err => this.setState({ searchResult: [], displayResult: 'none' }))
        }, 1000);
    }

    searchBarFocusOut = () => {
        if (!this.state.searchResultHover)
            this.setState({ displayResult: 'none' })
    }

    menuIsVisible = (isVisible) => {
        this.setState({ menuPosition: isVisible ? 'static' : 'fixed' })
    }

    render() {
        return (
            <nav class="navbar navbar-expand-lg navbar-light bg-white sticky-top py-0 px-5 main-menu">
                <Link class="navbar-brand" to="/"><img style={{ width: '150px' }} src={logo} alt="Logo" /></Link>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <i className="list icon" />
                </button>

                <div class="collapse navbar-collapse d-md-none" id="navbarSupportedContent">
                    <ul class="navbar-nav d-md-none">
                        <li class="nav-item">
                            <Link class="nav-link text-white" onClick={this.goToChat}>
                                <i aria-hidden="true" className="bell outline icon link-icon"></i>Message
                    </Link>
                        </li>
                        <li class="nav-item">
                            <Link className="nav-link text-white" to={'/profile/' + localStorage.getItem('profile_name')}>
                                <i aria-hidden="true" className="address book outline icon link-icon"></i>Profile
                    </Link>
                        </li>
                        <li class="nav-item">
                            <Link class="nav-link text-white" onClick={this.handleLogout}>
                                <i aria-hidden="true" className="sign-out icon link-icon" />Logout
                    </Link>
                        </li>
                    </ul>
                </div>

                <div class="collapse navbar-collapse" id="">
                    <div class="navbar-nav ml-auto mr-0">

                        <div class="nav-item">
                            <div className="ui icon input mx-2 px-5">
                                <div className="ui search">
                                    <div style={{ padding: ".78em 0" }} className="ui icon input">
                                        <input value={this.state.searchQuery} className='prompt' onBlur={this.searchBarFocusOut} onChange={this.searchAutocomplete} style={{ minWidth: '20em' }} type="text" placeholder="Search..." />
                                        <i aria-hidden="true" className="search icon"></i>
                                    </div>
                                    <div onMouseOver={() => this.setState({ searchResultHover: true })}
                                        onMouseOut={() => this.setState({ searchResultHover: false })}
                                        style={{ display: this.state.displayResult }} role="list" className="results">
                                        {this.state.searchResult.map((item, index) => {
                                            return (
                                                <div key={"key-" + index} onClick={() => {
                                                    this.setState({ displayResult: 'none', searchQuery: '' });
                                                    this.props.history.push('/profile/' + item.username);
                                                }} className="result">
                                                    <div className="image">
                                                        <img className='avatar' src={require('../Assets/images/avatar.jpg')} />
                                                    </div>
                                                    <div className="content">
                                                        <div className="title">{item.first_name + ' ' + item.last_name}</div>
                                                        <div className="description">{item.profile_name}</div>
                                                    </div>
                                                </div>
                                            )
                                        })}
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="nav-item dropdown mx-2">
                            <a class="nav-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                                <img className="ui navbar-avatar image" src={require('../Assets/images/avatar.jpg')} />
                            </a>
                            <div class="dropdown-menu dropdown-menu-right py-0" style={{ backgroundColor: 'rgb(252, 240, 228)' }}>
                                <h4 class="dropdown-header border-bottom text-center">Welcome, {localStorage.getItem('profile_name')}!</h4>
                                <Link to={'/profile/' + localStorage.getItem('profile_name')} className='dropdown-item'>
                                    <i aria-hidden="true" className="address book outline icon link-icon"></i>Profile
                                </Link>
                                <Link className='dropdown-item' onClick={this.handleLogout}>
                                    <i aria-hidden="true" className="sign-out icon link-icon"></i>Logout
                                </Link>
                            </div>
                        </div>
                    </div>
                </div>
            </nav>
        )
    }
}