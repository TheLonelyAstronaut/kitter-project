import React, {Component} from 'react';
import logo from '../Assets/images/logo3.png';
import * as AuthAPI from '../API/Auth'

export default class SignUp extends Component {
    constructor(props) {
      super(props);
      this.state = {
        email: '',
        password: '',
        password2: '',
        first_name: '',
        last_name: '',
        login: '',
        step: 1,
        error: null
      }
    }

    UNSAFE_componentWillMount() {
      localStorage.clear();
    }

    handleStep = (step) => {
        switch(step) {
            case 1:
                return (
                    <div className="field required">
                        <label>Login</label>
                        <div className="ui input input-wrapper">
                            <div className='input-icon'><i aria-hidden="true" className="user icon"></i></div>
                            <input onKeyPress={this.keyPressed} style={{border: 'None'}} placeholder="Login" value={this.state.login} onChange={e => this.setState({login: e.target.value})} />
                        </div>
                    </div>
                );
            case 2:
                return (
                    <div className="field required">
                        <label>Password</label>
                        <div className="ui input input-wrapper">
                            <div className='input-icon'><i aria-hidden="true" className="lock icon"></i></div>
                            <input onKeyPress={this.keyPressed} type='password' style={{border: 'None'}} placeholder="Password" value={this.state.password} onChange={e => this.setState({password: e.target.value})} />
                        </div>
                    </div>
                )
            case 3:
                return (
                    <div className="field required">
                        <label>Repeat Your Password</label>
                        <div className="ui input input-wrapper">
                            <div className='input-icon'><i aria-hidden="true" className="lock icon"></i></div>
                            <input onKeyPress={this.keyPressed} type='password' style={{border: 'None'}} placeholder="Password" value={this.state.password2} onChange={e => this.setState({password2: e.target.value})} />
                        </div>
                    </div>
                )
            case 4:
                return (
                    <div className="field required">
                        <label>First Name</label>
                        <div className="ui input input-wrapper">
                            <div className='input-icon'><i aria-hidden="true" className="user circle icon"></i></div>
                            <input onKeyPress={this.keyPressed} style={{border: 'None'}} placeholder="First Name" value={this.state.first_name} onChange={e => this.setState({first_name: e.target.value})} />
                        </div>
                    </div>
                )
            case 5:
                return (
                    <div className="field required">
                        <label>Last Name</label>
                        <div className="ui input input-wrapper">
                            <div className='input-icon'><i aria-hidden="true" className="user circle icon"></i></div>
                            <input onKeyPress={this.keyPressed} style={{border: 'None'}} placeholder="Last Name" value={this.state.last_name} onChange={e => this.setState({last_name: e.target.value})} />
                        </div>
                    </div>
                )
            case 6:
                return (
                    <div className="field required">
                        <label>Email</label>
                        <div className="ui input input-wrapper">
                            <div className='input-icon'><i aria-hidden="true" className="user icon"></i></div>
                            <input onKeyPress={this.keyPressed} style={{border: 'None'}} placeholder="Email" value={this.state.email} onChange={e => this.setState({email: e.target.value})} />
                        </div>
                    </div>
                );
        }
    }

    nextStep = () => {
        let fields = ['', 'login', 'password', 'password2', 'first_name', 'last_name', 'email'];

        if((this.state[fields[this.state.step]] === '' && this.state.step < 7)) this.setState({error: `This field is required!`});
        else {
            this.setState({error: null});
            if (this.state.step == 3 && this.state.password != this.state.password2) this.setState({error: `Password doesn't match`});
            else this.setState({step: this.state.step + 1, error: null})
        }
    }

    prevStep = () => {
        this.setState({step: this.state.step - 1, error: null})
    }
  
    handleSubmit = async () => {
      if (this.state.step === 7) {

        await AuthAPI.register(this.state.login, this.state.email, this.state.password, this.state.first_name, this.state.last_name)
        .then(res => {
            if(res.statusCode === 200) {
                this.props.history.push('/login');
            }
        })
        .catch(err => {
            this.setState({error: 'Profile name already exists!'})
        })
      }
    }

    keyPressed = (e) => {
        if (e.key === "Enter") {
            if(this.state.step !== 9) this.nextStep();
            else this.handleSubmit();
        }
      }
  
    render() {
        const nextButton = this.state.step == 7 ? (
                <div className="ui two column grid">
                    <div className="column">
                        <button type="button" onClick={this.handleSubmit} className="ui blue button login-form-buttons">Register</button>
                    </div>
                    <div className="column">
                        <button type='button' style={{marginTop: '10px'}} onClick={this.prevStep} className="ui button login-form-buttons pink">Back</button>
                    </div>
                </div>
            ) :
            this.state.step == 1 ? (
                    <button type='button' style={{marginTop: '10px'}} onClick={this.nextStep} className="ui button login-form-buttons pink">Continue</button>
                ) : (
                    <div className="ui two column grid">
                        <div className="column">
                            <button type='button' style={{marginTop: '10px'}} onClick={this.nextStep} className="ui button login-form-buttons pink">Continue</button>
                        </div>
                        <div className="column">
                            <button type='button' style={{marginTop: '10px'}} onClick={this.prevStep} className="ui button login-form-buttons pink">Back</button>
                        </div>
                    </div>
            );

        const errorMessage = this.state.error != null ? ( 
            <div className="ui error message">
                <div className="content">
                    <p style={{fontSize: '1rem'}}>{this.state.error}</p>
                </div>
            </div>
        ) : null;

        return (
            <div className="register-screen">
                <div className="ui text container register-wrapper">
                    <div className="ui card login-card">
                        <div className="content login-form-header">
                            <img className='login-form-logo' src={logo} alt="Logo" />
                        </div>
                        <div className="content">
                            <form onSubmit={(e) => e.preventDefault() } className="ui form">
                                {this.handleStep(this.state.step)}
                                {nextButton}
                            </form>
                            {
                                errorMessage
                            }
                            <div style={{textAlign:'center'}}>
                                <div className="ui horizontal divider">Or</div>
                                <button onClick={() => this.props.history.push('/login')} className="ui button login-form-buttons orangeReg">
                                    <i aria-hidden="true" className="sign-in icon"></i>Log in
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
  }