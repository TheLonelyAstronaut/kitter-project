import React, {Component} from 'react';
import Navbar from './Navbar'
import PostList from './PostList'
import SideBar from './Sidebar'
import * as AuthAPI from '../API/Auth'
import * as PostsAPI from '../API/Posts'

export default class Home extends Component {
    constructor(props) {
      super(props);
      this.state = {
          email: '',
          text_content: null,
          user_posts: null,
          userData: null,
          userAvatar: null,
          followings: []
      }
    }

    async componentDidMount() {
        const login = localStorage.getItem('login');
        const token = localStorage.getItem('token');
        
        AuthAPI.loginByToken(token, login)
        .then(res => {
            if (res.statusCode != 200) {
                localStorage.clear();
                this.props.history.push('/login');
            }
            else {
                this.requestPosts();
            }
        }).catch(err => {})
    }

    requestPosts = async () => {
        const login = localStorage.getItem('login');
        const token = localStorage.getItem('token');

        PostsAPI.getFeed(token, login)
        .then(res => {
            if (res.statusCode === 200){
                res = res.posts;
                let posts = [];

                if(res) {
                    res.forEach(post => {
                        let newPost = post[0];
                        newPost.likes = post[1];
                        newPost.liked = post[2];
                        posts.push(newPost);
                    })
                }

                this.setState({user_posts: posts});
            }
        })
        .catch(err => {})
    }

    render() {
        return (
            <div className='background'>
                <Navbar userData={this.state.userData} history={this.props.history}/>
                <div className='feed-container'>
                    <div className='row'>
                    <div className='d-none d-md-block col-lg-3 feed-column'>
                        <SideBar userData={this.state.userData} activeClass="home-item" />
                    </div>
                    <div className='col-sm-12 col-lg-9 feed-column'>
                        <div>
                            <PostList userAvatar = {this.state.userAvatar} posts={this.state.user_posts} isHomePage={true} allowToPost={true} requestPosts={this.requestPosts} />
                        </div>
                    </div>
                    </div>
                </div>
            </div>
        );
    }
  }